import { Injectable } from '@angular/core';

@Injectable()
export class AppVariables {

    private Language: string;

    constructor() { }


    GetLanguage(): string {
        let ReturnedValue: string;
        ReturnedValue = this.Language;
        return ReturnedValue;
    }

    SetLanguage(Data: string) {
        this.Language = Data;
    }

}
