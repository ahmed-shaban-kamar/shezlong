import { HttpHeaderProvider } from './http-header-provider';
import { Injectable } from '@angular/core';
import { map, timeout } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class HttpServicesProvider {

  constructor(
    public http: HttpClient,
    private httpHeaderProvider: HttpHeaderProvider,
  ) { }

  SendQuestionnaireData(object): Observable<any> {
    return this.http.post('service_url', object, { headers: this.httpHeaderProvider.getHeaderJson() })
      .pipe(timeout(environment.ApplicationHttpServiceTimeout), map(Response => {
        return Response;
      }));
  }

}
