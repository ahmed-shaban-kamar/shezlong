import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class HttpHeaderProvider {

    constructor() { }


    getHeaderJson() {
        const contentHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            // tslint:disable-next-line: object-literal-key-quotes
            'Accept': 'application/json'
        });
        return contentHeaders;
    }

}
