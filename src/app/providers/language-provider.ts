import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../environments/environment';
import { AppVariables } from '../app.variables';

@Injectable()
export class LanguageProvider {

    constructor(private translate: TranslateService, private appVariables: AppVariables) { }

    ApplicationStartLanguage() {
        if (localStorage.getItem(environment.ApplicationName + '-' + 'Language') === null) {
            this.ChangeLanguageBypassingLanguage('English');
        } else if (localStorage.getItem(environment.ApplicationName + '-' + 'Language') === 'Arabic') {
            this.ChangeLanguageBypassingLanguage('Arabic');
        } else if (localStorage.getItem(environment.ApplicationName + '-' + 'Language') === 'English') {
            this.ChangeLanguageBypassingLanguage('English');
        }
    }

    ChangeLanguageBypassingLanguage(Language) {
        localStorage.setItem(environment.ApplicationName + '-' + 'Language', Language);
        this.appVariables.SetLanguage(Language);
        this.translate.setDefaultLang(Language);
        this.translate.use(Language);

        if (Language === 'Arabic') {
            document.body.setAttribute('dir', 'rtl');
            document.body.style.textAlign = 'right';

        } else if (Language === 'English') {
            document.body.setAttribute('dir', 'ltr');
            document.body.style.textAlign = 'left';

        }
    }

    ReturnValueBasedOnLanguage(ArabicValue: string, EnglishValue: string): string {
        let ReturnedValue: string;
        if (this.appVariables.GetLanguage() === 'Arabic') {
            ReturnedValue = ArabicValue;
        } else if (this.appVariables.GetLanguage() === 'English') {
            ReturnedValue = EnglishValue;
        }
        return ReturnedValue;
    }

}
