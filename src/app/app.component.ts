import { Component } from '@angular/core';
import { LanguageProvider } from './providers/language-provider';
import { AppVariables } from './app.variables';
import Swal from 'sweetalert2';
import { HttpServicesProvider } from './providers/http-services/http-services-provider';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  CurrentStep = 1;
  Steps = [1, 2, 3, 4, 5, 6, 7];

  QestionsAndAnswers = [
    { Question: { Arabic: 'من هوا المريض', English: 'Who is the patient' }, Answers: [{ AnswerId: 1, Arabic: 'انا', English: 'Me' }, { AnswerId: 2, Arabic: 'ابنى', English: 'My son' }, { AnswerId: 3, Arabic: 'شخص اخر', English: 'Someone else' }], selectedAnswer: null },
    { Question: { Arabic: 'السؤال الثانى', English: 'Second question' }, Answers: [{ AnswerId: 4, Arabic: 'واحد', English: 'One' }, { AnswerId: 5, Arabic: 'اثنين', English: 'Two' }, { AnswerId: 6, Arabic: 'ثلاثة', English: 'Three' }], selectedAnswer: null },
    { Question: { Arabic: 'السؤال الثالث', English: 'Third question' }, Answers: [{ AnswerId: 7, Arabic: 'واحد', English: 'One' }, { AnswerId: 8, Arabic: 'اثنين', English: 'Two' }, { AnswerId: 9, Arabic: 'ثلاثة', English: 'Three' }], selectedAnswer: null },
    { Question: { Arabic: 'السؤال الرابع', English: 'Fourth question' }, Answers: [{ AnswerId: 10, Arabic: 'واحد', English: 'One' }, { AnswerId: 11, Arabic: 'اثنين', English: 'Two' }, { AnswerId: 12, Arabic: 'ثلاثة', English: 'Three' }], selectedAnswer: null },
    { Question: { Arabic: 'السؤال الخامس', English: 'Fifth question' }, Answers: [{ AnswerId: 13, Arabic: 'واحد', English: 'One' }, { AnswerId: 14, Arabic: 'اثنين', English: 'Two' }, { AnswerId: 15, Arabic: 'ثلاثة', English: 'Three' }], selectedAnswer: null },
    { Question: { Arabic: 'السؤال السادس', English: 'Sixth question' }, Answers: [{ AnswerId: 16, Arabic: 'واحد', English: 'One' }, { AnswerId: 17, Arabic: 'اثنين', English: 'Two' }, { AnswerId: 18, Arabic: 'ثلاثة', English: 'Three' }], selectedAnswer: null },
    { Question: { Arabic: 'ارسال بيانات الاستبيان', English: 'Send questionnaire data' }, Answers: [{ AnswerId: 0, Arabic: 'ارسال البيانات', English: 'Send data' }], selectedAnswer: null },
  ];

  constructor(public languageProvider: LanguageProvider, private appVariables: AppVariables, private httpServicesProvider: HttpServicesProvider) {
    this.languageProvider.ApplicationStartLanguage();
  }

  ChangeToNextLanguage() {
    if (this.appVariables.GetLanguage() === 'Arabic') {
      this.languageProvider.ChangeLanguageBypassingLanguage('English');
    } else if (this.appVariables.GetLanguage() === 'English') {
      this.languageProvider.ChangeLanguageBypassingLanguage('Arabic');

    }
  }

  ReturnStep(Step) {
    if (Step !== 7) {
      return Step;
    }
  }

  ReturnPercentage() {
    const Steb = 100 / this.QestionsAndAnswers.length;

    let AmountOfData = 0;
    this.QestionsAndAnswers.forEach(object => {
      if (object.selectedAnswer !== null) {
        AmountOfData = AmountOfData + 1;
      }
    });
    return (AmountOfData * Steb) + '%';
  }

  selectAnswer(AnswerId) {
    if (this.CurrentStep !== 7) {
      this.QestionsAndAnswers[this.CurrentStep - 1].selectedAnswer = AnswerId;
      this.CurrentStep = this.CurrentStep + 1;
    } else {
      this.SendData();
    }
  }

  GoToPagination(Index) {


    if ((Index !== 0 && this.QestionsAndAnswers[Index - 1].selectedAnswer !== null) || (Index === 0)) {
      this.CurrentStep = Index + 1;
    } else {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer);
          toast.addEventListener('mouseleave', Swal.resumeTimer);
        }
      });
      Toast.fire({
        icon: 'warning',
        title: this.languageProvider.ReturnValueBasedOnLanguage('من فضلك اختر من الحالى', 'Please choose from current')
      });
    }
  }

  SendData() {
    Swal.fire({
      text: this.languageProvider.ReturnValueBasedOnLanguage('هل انت متاكد من ارسال البيانات', 'Are you sure from sending data'),
      icon: 'warning',
      confirmButtonColor: '#529faf',
      cancelButtonColor: '#c5c5c5',
      showCancelButton: true,
      confirmButtonText: this.languageProvider.ReturnValueBasedOnLanguage('ارسال', 'Send'),
      cancelButtonText: this.languageProvider.ReturnValueBasedOnLanguage('الغاء', 'Cancel'),
    }).then((result) => {
      if (result.value) {
        this.SendQuestionnaireData();
      }
    });
  }


  SendQuestionnaireData() {
    const QuestionnaireData = [];
    this.QestionsAndAnswers.forEach(object => {
      if (object.selectedAnswer !== undefined) {
        QuestionnaireData.push(object.selectedAnswer);
      }
    });


    // unhash blow code to send it with http


    // this.httpServicesProvider.SendQuestionnaireData(QuestionnaireData).subscribe(
    //   data => {
    //     const Toast = Swal.mixin({
    //       toast: true,
    //       position: 'top-end',
    //       showConfirmButton: false,
    //       timer: 3000,
    //       timerProgressBar: true,
    //       onOpen: (toast) => {
    //         toast.addEventListener('mouseenter', Swal.stopTimer);
    //         toast.addEventListener('mouseleave', Swal.resumeTimer);
    //       }
    //     });
    //     Toast.fire({
    //       icon: 'success',
    //       title: this.languageProvider.ReturnValueBasedOnLanguage('تم ارسال البيانات بنجاح', 'Data has been sent')
    //     });
    //   }
    // );
  }
}
